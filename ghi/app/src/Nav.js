import { NavLink, Link } from 'react-router-dom';


function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Services
              </a>
              <ul className="dropdown-menu">
                <li><Link className="dropdown-item" to="technicians/new">New Technician</Link></li>
                <li><Link className="dropdown-item" to="service_appointment/">Service Appointments List</Link></li>
                <li><Link className="dropdown-item" to="service_appointment/new/">New Service Appointment</Link></li>
              </ul>
          </li>
          <li className='nav-item dropdown'>
              <a className='nav-link dropdown-toggle' href='#' role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul className='dropdown-menu'>
                  <li><Link className="dropdown-item" to="sales/performance">Salesman Performance</Link></li>
                  <li><Link className="dropdown-item" to="sales/new/">Create A Sale</Link></li>
                  <li><Link className="dropdown-item" to="sales/customer">Add a Customer</Link></li>
                  <li><Link className="dropdown-item" to="sales/employee">Add a Salesman</Link></li>
              </ul>
          </li>
          <li className='nav-item dropdown'>
            <a className='nav-link dropdown-toggle' href='#' role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
            </a>
              <ul className='dropdown-menu'>
                  <li><Link className="dropdown-item" to="sales/automobiles">Inventory List</Link></li>
                  <li><Link className="dropdown-item" to="sales/automobiles/new/">Add an Automobile</Link></li>
                  <li><Link className="dropdown-item" to="sales/models">Model List</Link></li>
                  <li><Link className="dropdown-item" to="sales/models/new">Add a Model</Link></li>
                  <li><Link className="dropdown-item" to="sales/manufacturers">Manufacturer List</Link></li>
                  <li><Link className="dropdown-item" to="sales/manufacturers/new">Add a Manufacturers</Link></li>
              </ul>
          </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
