import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './ServiceJS/EnterTechnicianForm';
import AppointmentForm from './ServiceJS/EnterAppointment';
import AppointmentList from './ServiceJS/ListOfAppointments';

import SaleForm from './SalesComponents/SaleForm';
import SalesList from './SalesComponents/SalesHistory';
import SalesPersonForm from './SalesComponents/NewEmployee';
import NewCustomerForm from './SalesComponents/NewCustomer';
import SalesSearch from './SalesComponents/SalesSearch';

import ManufacturerList from './InventoryComponents/ManufacturerList';
import ModelList from './InventoryComponents/ModelList';
import AutoList from './InventoryComponents/AutomobileList';
import ManufacturerForm from './InventoryComponents/NewManufacturer';
import VehicleModelForm from './InventoryComponents/NewModel';
import NewAutoForm from './InventoryComponents/NewAutomobile';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path = "technicians/new" element={<TechnicianForm />} />

          <Route path = "service_appointment/">
            <Route index element={<AppointmentList />} />
            <Route path = "new/" element={<AppointmentForm />} />
          </Route>

          <Route path="/sales/new" element={<SaleForm />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/sales/customer" element={<NewCustomerForm />} />
          <Route path="/sales/employee" element={<SalesPersonForm />} />
          <Route path="/sales/performance" element={<SalesSearch />} />

          <Route path="/sales/manufacturers" element={<ManufacturerList />} />
          <Route path="/sales/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/sales/models" element={<ModelList />} />
          <Route path="/sales/models/new" element={<VehicleModelForm />} />
          <Route path="/sales/automobiles" element={<AutoList />} />
          <Route path="/sales/automobiles/new" element={<NewAutoForm />} />


        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
