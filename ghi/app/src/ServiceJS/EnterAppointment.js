import React from 'react';

class AppointmentForm extends React.Component {
  state = {
          customer_name: '',
          tech_name: '',
          technicians: [],
          appointment_date: '',
          appointment_time: '',
          service_reason: '',
          vin: '',
          vip: '',
      };

  handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    this.setState({[name]: value});
  }

  handleSubmit = async (e) => {
      e.preventDefault();
      const data = {
        cutomer_name: this.state.customer_name,
        tech_name: this.state.tech_name,
        technicians: this.state.technicians,
        appointment_date: this.state.appointment_date,
        appointment_time: this.state.appointment_time,
        service_reason: this.state.service_reason,
        vin: this.state.vin,
        vip: this.state.vip

      }

      const serviceURL = 'http://localhost:3000/service_appointment/new/';
      const fetchConfig = {
          method: 'POST',
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          }
      };
      const response = await fetch(serviceURL, fetchConfig);
      if (response.ok) {

        this.setState({
              customer_name: '',
              tech_name: '',
              appointment_date: '',
              appointment_time: '',
              service_reason: '',
              vin: '',
              vip: '',
        });
      }
  }

  async componentDidMount() {
      const technicians = "http://localhost:8080/api/technicians/";
      const response = await fetch(technicians);
      if (response.ok) {
          const data = await response.json();
          this.setState({technicians: data.technician });
      }
  }

render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Enter a Service Appointment</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.customer_name} placeholder="Customer Name" required type="text" name="customer_name" id="name" className="form-control" />
                <label htmlFor="name">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <select onChange={this.handleChange} value={this.state.tech_name} placeholder="Tech Name" required name="tech_name" id="tech name" className="form-select">
                    <option value="defaultValue">Choose a Tech</option>
                    {console.log(this.state)}
                    {this.state.technicians.map(technician => {
                        return (
                            <option key={technician.id} value={technician.employee_number}>{technician.tech_name}</option>
                        )
                    })}
                </select>
                </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.appointment_date} placeholder="Ends" required type="date" name="appointment_date" id="appointment date" className="form-control" />
                <label htmlFor="appointment_date">Appointment Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.appointment_time} placeholder="Appointment Time" required type="text" id="appointment time" name="appointment_time" className="form-control" />
                <label htmlFor="appointment_time">Appointment Time</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.service_reason} placeholder="Service Reason" required type="text" name="service_reason" id="Service Reason" className="form-control" />
                <label htmlFor="service_reason">Service Reason</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.vip} placeholder="VIP" required type="text" name="vip" id="vip" className="form-control" />
                <label htmlFor="vip">VIP?</label>
              </div>
              <button className="btn btn-success">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AppointmentForm;