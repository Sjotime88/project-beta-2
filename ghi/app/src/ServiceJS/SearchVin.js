const SearchVIN = ({ search, setSearch }) => {
return (
    <form className ='searchForm' onSubmit={(e) => e.preventDefault()}>
        <label htmlFor='search'><strong>Search Service History</strong></label>
        <input
            id='search'
            type='text'
            role='searchbox'
            placeholder="Enter VIN"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            />
    </form>
    )
}

export default SearchVIN