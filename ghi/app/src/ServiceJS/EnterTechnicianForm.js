import React from 'react';

class TechnicianForm extends React.Component {
    state = {
        tech_name: '',
        employee_number: '',
        // state names need to match 'name'=tech_name in jsx. Which in turn need to match up with model names. Also need to match value={this.state.tech_name}
    }

    // Custom methods changed to annonymous functions mean we no longer have to bind
    handleChange = (e) => {
        const value = e.target.value;
        const name = e.target.name;
        this.setState({[name]: value});
        // looking for the value of name and assign it a value
    }

    handleSubmit = async (e) => {
        e.PreventDefault();
        const data = {
            tech_name: this.state.tech_name,
            employee_number: this.state.employee_number
        }
        const technicianURL = 'http://localhost:3000/technicians/new/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json',
            }
        };
        const response = await fetch(technicianURL, fetchConfig);
        if (response.ok) {
            // const newTechnician = await response.json();
            const clearForm = {
                tech_name: '',
                employee_number: '',
            };
            this.setState(clearForm);
            }
        }

    render() {
        return (
            <div className="row">
                <div className="offset-2 col-7">
                    <div className="shadow p-4 mt-4">
                        <h4>Create A New Service Technician</h4>
                        <form onSubmit={this.handleSubmit} id="create-service-technician">
                            <div className="form-floating mb-4">
                                <input onChange={this.handleChange} value={this.state.tech_name} placeholder="Technician Name" required type="text" name="tech_name" className="form-control" />
                                <label htmlFor="tech_name">Technician Name</label>
                            </div>
                            <div className="form-floating mb-4">
                                <input onChange={this.handleChange} value={this.state.employee_number} placeholder="Employee Number" required type="text" name="employee_number" className="form-control" />
                                <label htmlFor="employee_number">Employee Number</label>
                            </div>
                        <button className="btn btn-success">Create</button>
                    </form>
                </div>
            </div>
        </div>
        );
    }
}
export default TechnicianForm